// q1
SELECT * from film
WHERE title LIKE '%n'
ORDER BY length DESC
LIMIT 10;

// q2
SELECT * from film
WHERE title LIKE '%n'
ORDER BY length ASC
OFFSET 5
LIMIT 5;

// q3
SELECT * from customer
WHERE store_id = 1
ORDER BY last_name DESC
LIMIT 4;